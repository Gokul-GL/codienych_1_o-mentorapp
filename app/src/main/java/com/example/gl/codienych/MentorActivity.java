package com.example.gl.codienych;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MentorActivity extends AppCompatActivity
{
    private Button btn,mbtn;
    private EditText udata;
    String demo="null",data1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mentor);
        btn=findViewById(R.id.gmsg);
        mbtn=findViewById(R.id.submit);
        udata=findViewById(R.id.editText);
        SharedPreferences preferences=getSharedPreferences("codienych",MODE_PRIVATE);
        final String name=preferences.getString("email",null);
        mbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 data1=udata.getText().toString().trim();
                String link="http://sasurieinfo.tech/app/api/event2020Controller.php?input="+data1+"&mode=getparticipant";
                getJSON(link);
                Intent intent=new Intent(MentorActivity.this,ParMarkActivity.class);
                startActivity(intent);


            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MentorActivity.this,GroupMessageActivity.class);
                intent.putExtra("name",name);
                startActivity(intent);
            }
        });
    }

    private void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                int flog=0;
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    int value=jsonArray.length();
                    String[] data = new String[jsonArray.length()];
                    String[] clgname= new String[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        data[i] = obj.getString("name");
                        clgname[i] = obj.getString("collegename");
                        demo = data[i];
                        Toast.makeText(MentorActivity.this, ""+demo, Toast.LENGTH_SHORT).show();
                        if ("null".equals(demo)) {
                            Toast.makeText(MentorActivity.this, "Please enter a correct data", Toast.LENGTH_SHORT).show();
                        } else {
                            if (flog == (value - 1)) {
                                Intent intent = new Intent(MentorActivity.this, ParMarkActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("key1", data[i]);
                                bundle.putString("key2", clgname[i]);
                                bundle.putString("key3", obj.getString("id"));
                                bundle.putString("key4", "demo");
                                intent.putExtras(bundle);
                                startActivity(intent);
                            } else {
                                //String data1 = udata.getText().toString().trim();
                                Intent intent = new Intent(MentorActivity.this, DublicateDataActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("key1", data1);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
                @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null)
                    {
                        sb.append(json + "\n");

                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }
}
