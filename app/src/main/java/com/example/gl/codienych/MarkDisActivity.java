package com.example.gl.codienych;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MarkDisActivity extends AppCompatActivity
{

    private ListView listView;
    private String cata;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_dis);

        listView=findViewById(R.id.mlist);
        Intent i = getIntent();
        cata=i.getStringExtra("cat");
        Toast.makeText(this, ""+cata, Toast.LENGTH_SHORT).show();
        String link="http://sasurieinfo.tech/app/api/event2020Controller.php?mode=result&catagory="+cata;
        getJSON(link);
    }
    private void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String>
        {
            private String suid,svalu,m1,m2,m3,total,ucid;
            String vn1,vn2;
            int a,b,fm1,fm2,fm3,sm1,sm2,sm3;
            boolean check=false;
             int t1,t2,ft;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
            @Override
            protected void onPostExecute(final String s)
            {
                try {
                    loadIntoListView(s);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            try {
                                JSONArray jsonArray = new JSONArray(s);
//                    String data = null;
//                    String uid = new String();
//                    String uclg = new String();
                            //    Toast.makeText(MarkDisActivity.this, ""+position, Toast.LENGTH_SHORT).show();
                        for (int i=0;i<jsonArray.length();i++) {
                            JSONObject obj = jsonArray.getJSONObject(position);
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject secondobj = jsonArray.getJSONObject(j);
                                if (obj.getString("id").equals(secondobj.getString("id")))
                                {
                                    check=true;
                                    ucid=obj.getString("id");
                                    vn1 = obj.getString("valuator");
                                    vn2 = secondobj.getString("valuator");
                                    t1 =Integer.parseInt(obj.getString("total"));
                                    t2 = Integer.parseInt(secondobj.getString("total"));
                                    fm1=Integer.parseInt(obj.getString("Non_Plagiarism"));
                                    fm2=Integer.parseInt(obj.getString("logical_Skills"));
                                    fm3=Integer.parseInt( obj.getString("Code_Optimaisation"));
                                    sm1 = Integer.parseInt(secondobj.getString("Non_Plagiarism"));
                                    sm2 = Integer.parseInt(secondobj.getString("logical_Skills"));
                                    sm3 = Integer.parseInt(secondobj.getString("Code_Optimaisation"));
                                    fm1=(fm1+sm1)/2;
                                    fm2=(fm2+sm2)/2;
                                    fm3=(fm3+sm3)/2;
                                    ft=(t1+t2)/2;
                                    m1= String.valueOf(fm1);
                                    svalu=vn1+","+vn2;
                                    m2= String.valueOf(fm2);
                                    m3= String.valueOf(fm3);
                                    total= String.valueOf(ft);

                                }


                            }
                        }
                        if (check) {
                            JSONObject obj1 = jsonArray.getJSONObject(position);
                            ucid = obj1.getString("id");
                            svalu = obj1.getString("valuator");
                            m1 = obj1.getString("Non_Plagiarism");
                            m2 = obj1.getString("logical_Skills");
                            m3 = obj1.getString("Code_Optimaisation");
                            total = obj1.getString("total");

                        }
                                Intent intent = new Intent(getApplicationContext(), ShowMarkActivity.class);
                                intent.putExtra("name", ucid);
                                intent.putExtra("valuator", svalu);
                                intent.putExtra("m1", m1);
                                intent.putExtra("m2", m2);
                                intent.putExtra("m3", m3);
                                intent.putExtra("total", total);
                                startActivity(intent);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    super.onPostExecute(s);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }
    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        String[] data = new String[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++)
        {
            JSONObject obj = jsonArray.getJSONObject(i);
            data[i] = obj.getString("participant");
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
        listView.setAdapter(arrayAdapter);
    }
}
