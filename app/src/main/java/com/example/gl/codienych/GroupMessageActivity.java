package com.example.gl.codienych;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class GroupMessageActivity extends AppCompatActivity
{
    private Toolbar mToolbar;
    private ImageButton SendMessageButton;
    private EditText UserMessageInput;
    private ScrollView mScrollView;
    private TextView displayTextMessage;
    private String currentGroupName,currentUserId,currentUserName,currentDate,currentTime;
    private DatabaseReference UsersRef,GroupNameRef,GroupMessageKeyRef;
    private String catagory;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_message);
        currentGroupName="Codienych";
        //currentGroupName=getIntent().getExtras().get("groupName").toString();
        GroupNameRef=FirebaseDatabase.getInstance().getReference().child("Groups").child(currentGroupName);

        Toast.makeText(this, currentGroupName, Toast.LENGTH_SHORT).show();

        InitializeFields();
        GetUserInfo();
        SendMessageButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                SaveMassageInfoToDatabase();
                UserMessageInput.setText("");
                mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
        SharedPreferences preferences=getSharedPreferences("codienych",MODE_PRIVATE);
        catagory =preferences.getString("cat",null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GroupNameRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                if (dataSnapshot.exists())
                {
                    DisplayMessage(dataSnapshot);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {

                if (dataSnapshot.exists())
                {
                    DisplayMessage(dataSnapshot);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    private void InitializeFields()
    {
        mToolbar=findViewById(R.id.grop_chat_bar_layout);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(currentGroupName);
        SendMessageButton=findViewById(R.id.send_message_button);
        UserMessageInput=findViewById(R.id.input_group_massage);
        displayTextMessage=findViewById(R.id.group_chat_text_display);
        mScrollView=findViewById(R.id.my_scroll_view);
    }
    private void GetUserInfo()
    {
        Intent intent=getIntent();
        if ("MENTOR".equals(catagory)) {
            currentUserName = intent.getStringExtra("name");
        } else if ("ROOT".equals(catagory) || "SUPERMENTOR".equals(catagory)) {
            currentUserName = intent.getStringExtra("name");
        } else {
            currentUserName = intent.getStringExtra("pname");
        }
        Toast.makeText(this, ""+currentUserName, Toast.LENGTH_SHORT).show();
    }
    private void SaveMassageInfoToDatabase()
    {
        String message=UserMessageInput.getText().toString();
        String messageKey=GroupNameRef.push().getKey();
        if (TextUtils.isEmpty(message))
        {
            Toast.makeText(this, "Please Write Message First..", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Calendar calForData= Calendar.getInstance();
            SimpleDateFormat currentDateFormat=new SimpleDateFormat("MMM dd,yyyy");
            currentDate=currentDateFormat.format(calForData.getTime());

            Calendar calForTime= Calendar.getInstance();
            SimpleDateFormat currentTimeFormat=new SimpleDateFormat("hh:mm a");
            currentTime=currentTimeFormat.format(calForTime.getTime());


            HashMap<String,Object> groupMessageKey=new HashMap<>();

            GroupNameRef.updateChildren(groupMessageKey);
            GroupMessageKeyRef=GroupNameRef.child(messageKey);

            HashMap<String,Object> messageInfomap=new HashMap<>();
            messageInfomap.put("name",currentUserName);
            messageInfomap.put("message",message);
            messageInfomap.put("date",currentDate);
            messageInfomap.put("time",currentTime);

            GroupMessageKeyRef.updateChildren(messageInfomap);
        }
    }
    private void DisplayMessage(DataSnapshot dataSnapshot)
    {
        Iterator iterator=dataSnapshot.getChildren().iterator();

        while (iterator.hasNext())
        {
            String chatDate=(String)((DataSnapshot)iterator.next()).getValue();
            String chatMessage=(String)((DataSnapshot)iterator.next()).getValue();
            String chatName=(String)((DataSnapshot)iterator.next()).getValue();
            String chatTime=(String)((DataSnapshot)iterator.next()).getValue();
            displayTextMessage.append(chatName +":\n" + chatMessage +"\n" + chatTime + "     " + chatDate + "\n\n\n");
            mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
        }

    }
}
