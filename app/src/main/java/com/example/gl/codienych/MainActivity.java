package com.example.gl.codienych;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    private Button pbt,cbt;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        pbt=findViewById(R.id.p);
        cbt=findViewById(R.id.c);
        pbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent pintent=new Intent(MainActivity.this,ParticipentActivity.class);
                startActivity(pintent);

            }
        });
        cbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent cintent=new Intent(MainActivity.this,ComunityLoginActivity.class);
                startActivity(cintent);


            }
        });

    }
}
