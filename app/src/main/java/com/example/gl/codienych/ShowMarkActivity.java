package com.example.gl.codienych;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowMarkActivity extends AppCompatActivity
{
    private TextView pidtv,vntv,nptv,lstv,cotv,ttv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_mark);
        pidtv=findViewById(R.id.mpid);
        vntv=findViewById(R.id.mvname);
        nptv=findViewById(R.id.mm1);
        lstv=findViewById(R.id.mm2);
        cotv=findViewById(R.id.mm3);
        ttv=findViewById(R.id.mtotal);
        Intent i = getIntent();
        pidtv.setText(i.getStringExtra("name"));
        vntv.setText(i.getStringExtra("valuator"));
        nptv.setText(i.getStringExtra("m1"));
        lstv.setText(i.getStringExtra("m2"));
        cotv.setText(i.getStringExtra("m3"));
        ttv.setText(i.getStringExtra("total"));
    }
}

