package com.example.gl.codienych;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RootActivity extends AppCompatActivity
{

    private Button interbtn,exterbtn,gmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        interbtn=findViewById(R.id.inbtn);
        exterbtn=findViewById(R.id.exbtn);
        gmsg=findViewById(R.id.gmsgbtn);
        SharedPreferences preferences=getSharedPreferences("codienych",MODE_PRIVATE);
        final String name=preferences.getString("email",null);

        interbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(RootActivity.this,MarkDisActivity.class);
                intent.putExtra("cat","INTERNAL");
                startActivity(intent);
            }
        });
        exterbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(RootActivity.this,MarkDisActivity.class);
                intent.putExtra("cat","EXTARNAL");
                startActivity(intent);
            }
        });
        gmsg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(RootActivity.this,GroupMessageActivity.class);
                intent.putExtra("name",name);
                startActivity(intent);
            }
        });
    }
}
