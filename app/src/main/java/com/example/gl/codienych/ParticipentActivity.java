package com.example.gl.codienych;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.acl.Group;
import java.util.Arrays;

public class ParticipentActivity extends AppCompatActivity
{
    private Button pbtn;
    private EditText data;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participent);

        data=findViewById(R.id.serch);
        pbtn=findViewById(R.id.psubmit);
        pbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String verif=data.getText().toString().trim();
                if (verif.equals(""))
                {
                    Toast.makeText(ParticipentActivity.this, "Please Enter the data", Toast.LENGTH_SHORT).show();
                }
               else {
                    Toast.makeText(ParticipentActivity.this, ""+verif, Toast.LENGTH_SHORT).show();
                    String link="http://sasurieinfo.tech/app/api/event2020Controller.php?input="+verif+"&mode=getparticipant";
                    getJSON(link);
                    Intent intent=new Intent(ParticipentActivity.this,GroupMessageActivity.class);
                    startActivity(intent);
                }

            }
        });

        SharedPreferences preferences=getSharedPreferences("codienych",MODE_PRIVATE);
        name=preferences.getString("name",null);
        if (name!=null)
        {
            startActivity(new Intent(ParticipentActivity.this, GroupMessageActivity.class));
            finish();
        }

    }
    private void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    String[] data = new String[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        data[i] = obj.getString("name");

                        SharedPreferences.Editor editor=getSharedPreferences("codienych",MODE_PRIVATE).edit();
                        editor.putString("name", Arrays.toString(data));
                        editor.apply();
                        Toast.makeText(ParticipentActivity.this, ""+data, Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getApplicationContext(),GroupMessageActivity.class);
                        in.putExtra("pname", data);
                        startActivity(in);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null)
                    {
                        sb.append(json + "\n");
                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }
}
