package com.example.gl.codienych;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

public class ComunityLoginActivity extends AppCompatActivity
{
private TextView tv;
private Button btn;
private EditText email,pass;
private String eid,pow;
private String catagory="demo";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comunity_login);

        tv=findViewById(R.id.textView);
        btn=findViewById(R.id.button);
        email=findViewById(R.id.name);
        pass=findViewById(R.id.cpass);

        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                eid=email.getText().toString().trim();
                pow=pass.getText().toString();
                if (eid.equals("")&&pow.equals(""))
                {
                    Toast.makeText(ComunityLoginActivity.this, "Please Enter a emailid and password", Toast.LENGTH_SHORT).show();
                }
                else {
                    String link="http://sasurieinfo.tech/app/api/event2020Controller.php?mode=committeeLogin&password="+pow+"&data="+eid;
                    getJSON(link);
                }
            }
        });
        tv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(ComunityLoginActivity.this,ComeRgisterActivity.class);
                startActivity(intent);
            }
        });
        SharedPreferences preferences=getSharedPreferences("codienych",MODE_PRIVATE);
        String name=preferences.getString("name",null);
       catagory =preferences.getString("cat",null);
        if (name!=null)
        {
            startActivity(new Intent(ComunityLoginActivity.this,MentorActivity.class));
            finish();
        }
        if (Objects.equals(catagory, "ROOT"))
        {
            startActivity(new Intent(ComunityLoginActivity.this,RootActivity.class));
            finish();
        }


    }
    private void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {
private String catc;
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                int flog=0;
                JSONArray jsonArray = null;
                try {
                    eid=email.getText().toString().trim();
                   // Toast.makeText(ComunityLoginActivity.this, ""+eid, Toast.LENGTH_SHORT).show();
                    jsonArray = new JSONArray(s);
                    String[] data = new String[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        data[i]= obj.getString("email");
                        catc=obj.getString("catagory");

                    String verifiydata=data[i];
                       // Toast.makeText(ComunityLoginActivity.this, ""+verifiydata, Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor=getSharedPreferences("codienych",MODE_PRIVATE).edit();
                         if (verifiydata.equals(eid))
                        {
                            if (catc.equals("ROOT"))
                            {
                                Intent intent=new Intent(ComunityLoginActivity.this,RootActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Intent intent=new Intent(ComunityLoginActivity.this,MentorActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            editor.putString("name",verifiydata);
                            editor.putString("email",obj.getString("name"));
                            editor.putString("id",obj.getString("id"));
                            editor.putString("cat",obj.getString("catagory"));
                            editor.apply();
                            Toast.makeText(ComunityLoginActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(ComunityLoginActivity.this, "Enter a correct email and password ", Toast.LENGTH_SHORT).show();
                        }
                        // Toast.makeText(MentorActivity.this, ""+data[i], Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null)
                    {
                        sb.append(json + "\n");

                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }

}
