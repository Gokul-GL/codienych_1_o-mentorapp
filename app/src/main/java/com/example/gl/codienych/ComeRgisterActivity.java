package com.example.gl.codienych;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

public class ComeRgisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    private Button btn;
    private Spinner spinner;
    private EditText unmae,cmail,phone,pass,uid;
    String item;
    private static final String[] paths = {"MENTOR", "SUPERMENTOR", "ROOT"};
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_come_rgister);
        spinner =findViewById(R.id.spinner);
        btn=findViewById(R.id.button2);
        uid=findViewById(R.id.uid);
        unmae=findViewById(R.id.uname);
        cmail=findViewById(R.id.cemail);
        phone=findViewById(R.id.ph_no);
        pass=findViewById(R.id.pass);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String nmae=uid.getText().toString().trim();
                if (nmae.equals("sit"))
                {
                    Intent intent=new Intent(ComeRgisterActivity.this,ComunityLoginActivity.class);
                    startActivity(intent);
                    finish();
                    new ComeRgisterActivity.DataAsyncTask().execute();
                }
                else {
                    Toast.makeText(ComeRgisterActivity.this, "Please Enter a correct user name...", Toast.LENGTH_SHORT).show();
                }

            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ComeRgisterActivity.this,android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        item = parent.getItemAtPosition(position).toString();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }
    public class DataAsyncTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... params) {
            try {

                String name=unmae.getText().toString();
                String password=pass.getText().toString();
                String mail=cmail.getText().toString();
                String contectno=phone.getText().toString();
                String cata=item;
                String postUrl="http://sasurieinfo.tech/app/api/event2020Controller.php?mode=createCommittee" +
                        "&name="+name+"&password="+password+"&email="+mail+"&contact="+contectno+"&catagory="+cata;
               HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(postUrl);
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity resEntity = response.getEntity();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
