package com.example.gl.codienych;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;

public class ParMarkActivity extends AppCompatActivity
{
    private Button btn;
    private EditText pname,pclgname,m1edt,m2edt,m3edt;
    private String puname,pclg,m1s,m2s,m3s,total;
    String mid,id,dpid;
    String demo="empty";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_par_mark);
        btn=findViewById(R.id.sbtn);
        pname=findViewById(R.id.pid);
        pclgname=findViewById(R.id.parname);
        m1edt=findViewById(R.id.m1);
        m2edt=findViewById(R.id.m2);
        m3edt=findViewById(R.id.m3);

        Bundle bundle = getIntent().getExtras();
        String name = null;
        String email=null;

        id=null;
        if (bundle != null) {
            name = bundle.getString("key1");
            email=bundle.getString("key2");
            id=bundle.getString("key3");
        }
        pname.setText(name);
        pclgname.setText(email);
        Intent i = getIntent();
        demo=i.getStringExtra("cid");
        if ("demo".equals(demo))
        {
            String pmname = i.getStringExtra("name");
            String uclg=i.getStringExtra("clgname");
            dpid=i.getStringExtra("pid");
            // displaying selected product name
            pname.setText(pmname);
            pclgname.setText(uclg);
            id=dpid;
        }
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                puname=pname.getText().toString();
                pclg=pclgname.getText().toString();
                m1s=m1edt.getText().toString();
                m2s=m2edt.getText().toString();
                m3s=m3edt.getText().toString();
                if (puname.equals("")&&pclg.equals("")&&m1s.equals("")&&m2s.equals("")&&m3s.equals(""))
                {
                    Toast.makeText(ParMarkActivity.this, "Please Must be fill the all details", Toast.LENGTH_SHORT).show();
                }else {
                    new ParMarkActivity.DataAsyncTask().execute();
                    Intent intent = new Intent(ParMarkActivity.this, MentorActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });


        SharedPreferences preferences=getSharedPreferences("codienych",MODE_PRIVATE);
        mid=preferences.getString("id",null);

    }

    public class DataAsyncTask extends AsyncTask<String, String, String> {

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {
                int m1,m2,m3,total;
                m1=Integer.parseInt(m1s);
                m2=Integer.parseInt(m2s);
                m3=Integer.parseInt(m3s);
                total=m1+m2+m3;
                String ftotal=Integer.toString(total);
                String postUrl="http://sasurieinfo.tech/app/api/event2020Controller.php?mode=committeeValue&uid="+id+"&vid="+mid+"&m1="+m1s+"&m2="+m2s+"&m3="+m3s+"&total="+ftotal;
                HttpClient httpClient = new DefaultHttpClient();
//// post header
                HttpPost httpPost = new HttpPost(postUrl);
//
//// add your data
//                String name=unmae.getText().toString();
//                String password=pass.getText().toString();
//                String mail=cmail.getText().toString();
//                String contectno=contect.getText().toString();
//               // fina = (RadioButton) group.findViewById(group.getCheckedRadioButtonId());
//                String cata=item;
//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//                nameValuePairs.add(new BasicNameValuePair("name", name));
//                nameValuePairs.add(new BasicNameValuePair("password",password));
//                nameValuePairs.add(new BasicNameValuePair("email",mail));
//                nameValuePairs.add(new BasicNameValuePair("contact", contectno));
//                nameValuePairs.add(new BasicNameValuePair("catagory",cata));
                //httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//// execute HTTP post request
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null)
                {
                    String responseStr = EntityUtils.toString(resEntity).trim();
                    Log.v("OP", "Response:     " + responseStr);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }



}
